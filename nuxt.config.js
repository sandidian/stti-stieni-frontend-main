import colors from 'vuetify/es5/util/colors'
require('dotenv').config()

export default {
  // mode: 'universal',
  // ssr: true,
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s | STTI-STIENI JAKARTA',
    title: 'STTI-STIENI JAKARTA',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'preconnect',
        href: 'https://fonts.gstatic.com',
      },
      {
        type: 'text/css',
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Montserrat:wght@900&display=swap',
      },
    ],
  },

  css: ['~/static/main.css'],
  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/vue-carousel', ssr: false },
    { src: '~/plugins/vue-light-gallery', ssr: false },
    { src: '~/plugins/vue-html2pdf', mode: 'client' },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // 'cookie-universal-nuxt'
    '@nuxtjs/auth-next',
  ],

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},

  server: {
    port: process.env.PORT || 3000,
    host: process.env.BASE_URL || 'localhost',
  },

  axios: {
    baseURL: process.env.BASE_URL_API,
  },

  auth: {
    redirect: {
      login: '/smb/login',
      logout: '/',
      callback: '/smb/login',
      home: false,
    },
    strategies: {
      local: {
        token: {
          property: 'token',
        },
        user: {
          property: 'data',
        },
        endpoints: {
          login: { url: '/api/v1/mahasiswa-baru/auth/login', method: 'post' },
          logout: { url: '/api/v1/mahasiswa-baru/auth/logout', method: 'get' },
          user: { url: '/api/v1/mahasiswa-baru/user', method: 'get' },
        },
      },
    },
  },

  loading: '~/components/LoadingBar.vue',
}
