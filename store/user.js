export const state = () => ({
  changePassword: null,
  updateProfile: null,
})

export const mutations = {
  CHANGE_PASSWORD(state, changePassword) {
    state.changePassword = changePassword
  },
  UPDATE_PROFILE(state, updateProfile) {
    state.updateProfile = updateProfile
  },
}

export const getters = {
  getChangePassword: (state) => {
    return state.changePassword
  },
  getUpdateProfile: (state) => {
    return state.updateProfile
  },
}

export const actions = {
  async changePassword({ commit, dispatch }, { data }) {
    // console.log(data)
    await this.$axios({
      method: 'post',
      url: '/api/v1/mahasiswa-baru/user/change-password',
      data: data,
    })
      .then(function (response) {
        console.log(response.data)
        if (response.status === 200 && response.data.success === true)
          commit('CHANGE_PASSWORD', response.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
  async updateProfile({ commit, dispatch }, { data }) {
    // console.log(id)
    await this.$axios({
      method: 'post',
      url: `/api/v1/mahasiswa-baru/user/update-profile`,
      data: data,
    })
      .then(function (response) {
        console.log(response.data)
        if (response.status === 200 && response.data.success === true)
          commit('UPDATE_PROFILE', response.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
}
