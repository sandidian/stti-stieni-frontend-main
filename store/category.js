export const state = () => ({
  category: null,
  detail: null,
})

export const mutations = {
  SET_CATEGORY(state, category) {
    state.category = category
  },
  SET_DETAIL(state, detail) {
    state.detail = detail
  },
}

export const actions = {
  async list({ commit, dispatch }, { type }) {
    await this.$axios({
      method: 'get',
      url: `/api/v1/category`,
      params: { type: type },
    })
      .then(function (response) {
        if (response.status === 200 && response.data.success === true)
          commit('SET_CATEGORY', response.data.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
  async detail({ commit, dispatch }, { type, id }) {
    await this.$axios({
      method: 'get',
      url: `/api/v1/category/edit/${id}`,
      params: { id: id, type: type },
    })
      .then(function (response) {
        if (response.status === 200 && response.data.success === true)
          commit('SET_DETAIL', response.data.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
}

export const getters = {
  getCategory: (state) => {
    return state.category
  },
  getDetail: (state) => {
    return state.detail
  },
}
