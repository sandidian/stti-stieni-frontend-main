export const state = () => ({
  study: null,
})

export const mutations = {
  SET_STUDY(state, study) {
    state.study = study
  },
}

export const actions = {
  async list({ commit, dispatch }) {
    await this.$axios
      .get(`/api/v1/study`)
      .then(function (response) {
        if (response.status === 200 && response.data.success === true)
          commit('SET_STUDY', response.data.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
}

export const getters = {
  getStudy: (state) => {
    return state.study
  },
}
