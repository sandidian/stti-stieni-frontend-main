export const state = () => ({
  article: null,
  detail: null,
  viewer: null,
})

export const mutations = {
  SET_ARTICLE(state, article) {
    state.article = article
  },
  SET_DETAIL(state, detail) {
    state.detail = detail
  },
  SET_VIEWER(state, viewer) {
    state.viewer = viewer
  },
}

export const actions = {
  async list({ commit, dispatch }, { params }) {
    // console.log(params)

    await this.$axios({
      method: 'get',
      url: '/api/v1/article',
      params: params,
    })
      .then(function (response) {
        if (response.status === 200 && response.data.success === true)
          commit('SET_ARTICLE', response.data.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
  async detail({ commit, dispatch }, { slug }) {
    // console.log(slug)
    try {
      var response = await this.$axios({
        method: 'get',
        url: `/api/v1/article/detail/${slug}`,
      })
      if (response.status === 200 && response.data.success === true)
        commit('SET_DETAIL', response.data.data)
      else throw new Error(response.data.message)
    } catch (error) {
      if (error.response === undefined) throw error
      else throw new Error('Network Communication Error')
    }
  },
  async addViewer({ commit, dispatch }, { slug }) {
    // console.log(slug)
    try {
      var response = await this.$axios({
        method: 'post',
        url: `/api/v1/article/add-viewer/${slug}`,
      })
      if (response.status === 200 && response.data.success === true)
        commit('SET_VIEWER', response.data.data)
      else throw new Error(response.data.message)
    } catch (error) {
      if (error.response === undefined) throw error
      else throw new Error('Network Communication Error')
    }
  },
}

export const getters = {
  getArticle: (state) => {
    return state.article
  },
  getDetail: (state) => {
    return state.detail
  },
  getViewer: (state) => {
    return state.viewer
  },
}
