export const state = () => ({
  event: null,
  detail: null,
})

export const mutations = {
  SET_EVENT(state, event) {
    state.event = event
  },
  SET_DETAIL(state, detail) {
    state.detail = detail
  },
}

export const actions = {
  async list({ commit, dispatch }, { params }) {
    await this.$axios({
      method: 'get',
      url: '/api/v1/event',
      params: params,
    })
      .then(function (response) {
        if (response.status === 200 && response.data.success === true)
          commit('SET_EVENT', response.data.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
  async detail({ commit, dispatch }, { slug }) {
    try {
      var response = await this.$axios({
        method: 'get',
        url: `/api/v1/event/detail/${slug}`,
      })
      if (response.status === 200 && response.data.success === true)
        commit('SET_DETAIL', response.data.data)
      else throw new Error(response.data.message)
    } catch (error) {
      if (error.response === undefined) throw error
      else throw new Error('Network Communication Error')
    }
  },
}

export const getters = {
  getEvent: (state) => {
    return state.event
  },
  getDetail: (state) => {
    return state.detail
  },
}
