export const state = () => ({
  data: null,
  resetPassword: null,
})

export const mutations = {
  SET_DATA(state, data) {
    state.data = data
  },
  SET_RESET_PASSWORD(state, resetPassword) {
    state.resetPassword = resetPassword
  },
}

export const getters = {
  getData: (state) => {
    return state.data
  },
  getResetPassword: (state) => {
    return state.resetPassword
  },
}

export const actions = {
  async find({ commit, dispatch }, { token }) {
    await this.$axios
      .get(`/api/v1/auth/reset-password/mahasiswa-baru/${token}`)
      .then(function (response) {
        // console.log(response)
        if (response.status === 200) commit('SET_DATA', response.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error(error)
      })
  },
  async reset({ commit, dispatch }, { data }) {
    // console.log(data)
    await this.$axios({
      method: 'post',
      url: '/api/v1/auth/reset-password',
      data: data,
    })
      .then(function (response) {
        console.log(response.data)
        if (response.status === 200 && response.data.success === true)
          commit('SET_RESET_PASSWORD', response.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
}
