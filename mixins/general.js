import Vue from 'vue'
import {
  STATUS,
  JURUSAN,
  TYPE_CATEGORY,
  STATUS_POST,
  WAKTU_KULIAH,
} from '@/utils/constants'
import moment from 'moment'
import 'moment/locale/id'

Vue.mixin({
  methods: {
    getType(data) {
      var condition = ''

      for (const item of TYPE_CATEGORY) {
        if (item.id === data) condition = item.name
      }
      return condition
    },
    getStatus(data) {
      var condition = ''

      for (const item of STATUS) {
        if (item.id === data) condition = item.text
      }
      return condition
    },
    getStatusPost(data) {
      var condition = ''

      for (const item of STATUS_POST) {
        if (item.id === data) condition = item
      }
      return condition
    },
    getWaktuKuliah(data) {
      var condition = ''

      for (const item of WAKTU_KULIAH) {
        if (item.id === data) condition = item
      }
      return condition
    },
    getJurusan(data) {
      var condition = {}

      for (const item of JURUSAN) {
        if (item.id === data) condition = item
      }
      return condition
    },
    getProvince(regencies, kota_id) {
      var id = 0
      var name = ''

      for (var item in regencies) {
        if (regencies[item].id === kota_id) {
          id = regencies[item].province.id
          name = regencies[item].province.name
        }
      }

      return {
        id,
        name,
      }
    },
    getDataRegion(regencies, kota_id) {
      let data = []

      for (var item in regencies) {
        if (regencies[item].id === kota_id) {
          data = regencies[item]
        }
      }

      return data
    },
    dateFull(data, format = null) {
      moment.locale('id')
      var defaultFormat = 'yyyy-MM-DD hh:mm:ss'

      if (format) {
        var defaultFormat = format
      }

      // return moment.utc(String(data)).format(defaultFormat)
      return moment(data).format(defaultFormat)
    },
  },
})
